#!/bin/sh

if test "$muninEnabled" -eq 1
then
JMOLE_PARAMS='-Djmole.protocol.munin.enabled=true'
fi

if test "$statsdEnabled" -eq 1
then
JMOLE_PARAMS='-Djmole.protocol.statsd.enabled=true'
fi

if test "$logstashEnabled" -eq 1
then
JMOLE_PARAMS='-Djmole.protocol.logstash.enabled=true'
fi

/opt/jboss/wildfly/bin/standalone.sh $JMOLE_PARAMS -c standalone-full-ha.xml
