<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">	

<measurements>

<xsl:for-each select="/java/object/void[@method='add']/object[@class='net.welen.jmole.Configuration']">

	<measurement name="{void[@property='mBeanCollector']/object/void[@property='name']/string}" category="{void[@property='presentationInformation']/object/void[@property='category']/string}" level="{void[@property='Level']/int}">

		<objectNameQueries>
	<xsl:for-each select="void[@property='mBeanFinder']/object/void[@property='objectNameQueries']/object/void">		
			<objectNameQuery><xsl:value-of select="string"/></objectNameQuery>
	</xsl:for-each>
		</objectNameQueries>

<xsl:if test="(count(void[@property='mBeanFinder']/object/void[@property='attributeName'])&gt;0) or (count(void[@property='mBeanFinder']/object/void[@property='attributeMatch'])&gt;0) or (count(void[@property='mBeanFinder']/object/void[@property='parameterName'])&gt;0) or (count(void[@property='mBeanFinder']/object/void[@property='parameterMatch'])&gt;0) or (count(void[@property='mBeanFinder']/object/void[@property='className'])&gt;0)">
		<filters>
	<xsl:if test="count(void[@property='mBeanFinder']/object/void[@property='attributeName'])&gt;0">
			<attributeName><xsl:value-of select="void[@property='mBeanFinder']/object/void[@property='attributeName']"/></attributeName>
	</xsl:if>			
	<xsl:if test="count(void[@property='mBeanFinder']/object/void[@property='attributeMatch'])&gt;0">
			<attributeMatch><xsl:value-of select="void[@property='mBeanFinder']/object/void[@property='attributeMatch']"/></attributeMatch>
	</xsl:if>			
	<xsl:if test="count(void[@property='mBeanFinder']/object/void[@property='parameterName'])&gt;0">
			<parameterName><xsl:value-of select="void[@property='mBeanFinder']/object/void[@property='parameterName']"/></parameterName>
	</xsl:if>			
	<xsl:if test="count(void[@property='mBeanFinder']/object/void[@property='parameterMatch'])&gt;0">
			<parameterMatch><xsl:value-of select="void[@property='mBeanFinder']/object/void[@property='parameterMatch']"/></parameterMatch>
	</xsl:if>			
	<xsl:if test="count(void[@property='mBeanFinder']/object/void[@property='className'])&gt;0">
			<className><xsl:value-of select="void[@property='mBeanFinder']/object/void[@property='className']"/></className>
	</xsl:if>			
		</filters>
</xsl:if>		
	
<xsl:if test="(count(void[@property='mBeanCollector']/object/void[@property='nameAttributes'])&gt;0) or (count(void[@property='mBeanCollector']/object/void[@property='nameParameters'])&gt;0)">													
		<nameExtensions>
	<xsl:if test="count(void[@property='mBeanCollector']/object/void[@property='nameParameters'])&gt;0">													
			<parameters>
		<xsl:for-each select="void[@property='mBeanCollector']/object/void[@property='nameParameters']/object/void">			
				<parameter><xsl:value-of select="string"/></parameter>
		</xsl:for-each>
			</parameters>
	</xsl:if>			
	<xsl:if test="count(void[@property='mBeanCollector']/object/void[@property='nameAttributes'])&gt;0">
			<attributes>
		<xsl:for-each select="void[@property='mBeanCollector']/object/void[@property='nameAttributes']/object/void">
				<attribute><xsl:value-of select="string"/></attribute>
		</xsl:for-each>
			</attributes>
	</xsl:if>			
		</nameExtensions>
</xsl:if>
		
		<unit><xsl:value-of select="void[@property='presentationInformation']/object/void[@property='unit']/string"/></unit>
 		
		<description><xsl:value-of select="void[@property='presentationInformation']/object/void[@property='description']/string"/></description>
 		
		<attributes>
	<xsl:for-each select="void[@property='mBeanCollector']/object/void[@property='attributes']/object/void">
		<xsl:variable name="name"><xsl:value-of select="string"/></xsl:variable>
			<attribute name="{$name}">
		<xsl:if test="count(../../../void[@property='dataCollectorExtractors']/object/void/string[text()=$name])&gt;0">
	 			<dataExtractor class="{../../../void[@property='dataCollectorExtractors']/object/void/string[text()=$name]/../object/@class}" realName="{../../../void[@property='dataCollectorExtractors']/object/void/string[text()=$name]/../object/void/string}"/>
		</xsl:if>
		<xsl:if test="count(../../../../../void[@property='presentationInformation']/object/void[@property='attributeLabels']/object/void/string[text()=$name]/../string[2])&gt;0">
				<label><xsl:value-of select="../../../../../void[@property='presentationInformation']/object/void[@property='attributeLabels']/object/void/string[text()=$name]/../string[2]"/></label>
		</xsl:if>			
		<xsl:if test="count(../../../../../void[@property='presentationInformation']/object/void[@property='attributeDescriptions']/object/void/string[text()=$name]/../string[2])&gt;0">
				<description><xsl:value-of select="../../../../../void[@property='presentationInformation']/object/void[@property='attributeDescriptions']/object/void/string[text()=$name]/../string[2]"/></description>
		</xsl:if>				
		<xsl:if test="count(../../../../../void[@property='mBeanCollector']/object/void[@property='counterIntervals']/object/void/string[text()=$name]/../int)&gt;0">
				<interval><xsl:value-of select="../../../../../void[@property='mBeanCollector']/object/void[@property='counterIntervals']/object/void/string[text()=$name]/../int"/></interval>
		</xsl:if>			
		<xsl:if test="(count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='WarningLowThreshold'])&gt;0) or (count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='WarningHighThreshold'])&gt;0) or (count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='CriticalLowThreshold'])&gt;0) or (count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='CriticalHighThreshold'])&gt;0)">
				<thresholds>
					<common>
					<xsl:if test="count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='WarningLowThreshold'])">					
						<warningLowThreshold><xsl:value-of select="../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='WarningLowThreshold']/string"/></warningLowThreshold>
					</xsl:if>	
					<xsl:if test="count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='WarningHighThreshold'])">
						<warningHighThreshold><xsl:value-of select="../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='WarningHighThreshold']/string"/></warningHighThreshold>			
					</xsl:if>	
					<xsl:if test="count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='CriticalLowThreshold'])">
						<criticalLowThreshold><xsl:value-of select="../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='CriticalLowThreshold']/string"/></criticalLowThreshold>
					</xsl:if>							
					<xsl:if test="count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='CriticalHighThreshold'])">
						<criticalHighThreshold><xsl:value-of select="../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='CriticalHighThreshold']/string"/></criticalHighThreshold>
					</xsl:if>										
					<xsl:if test="count(../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='message'])">
						<message><xsl:value-of select="../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='message']/string"/></message>
					</xsl:if>										
					</common>
				<xsl:for-each select="../../../../../void[@property='Thresholds']/object/void/string[text()=$name]/../object/void[@property='IndividualThresholds']/object/void">
					<individual match="{string}">
					<xsl:if test="count(object/void[@property='WarningLowThreshold'])">
						<warningLowThreshold><xsl:value-of select="object/void[@property='WarningLowThreshold']/string"/></warningLowThreshold>
					</xsl:if>
					<xsl:if test="count(object/void[@property='WarningHighThreshold'])">
						<warningHighThreshold><xsl:value-of select="object/void[@property='WarningHighThreshold']/string"/></warningHighThreshold>
					</xsl:if>
					<xsl:if test="count(object/void[@property='CriticalLowThreshold'])">
						<criticalLowThreshold><xsl:value-of select="object/void[@property='CriticalLowThreshold']/string"/></criticalLowThreshold>
					</xsl:if>											
					<xsl:if test="count(object/void[@property='CriticalHighThreshold'])">
						<criticalHighThreshold><xsl:value-of select="object/void[@property='CriticalHighThreshold']/string"/></criticalHighThreshold>
					</xsl:if>						
					<xsl:if test="count(object/void[@property='message'])">
						<message><xsl:value-of select="object/void[@property='message']/string"/></message>
					</xsl:if>						
					</individual>
				</xsl:for-each>
				</thresholds>
		</xsl:if>
			</attribute>
	</xsl:for-each>
		</attributes>		

	</measurement>

</xsl:for-each>
	
</measurements>

</xsl:template>
</xsl:stylesheet>
