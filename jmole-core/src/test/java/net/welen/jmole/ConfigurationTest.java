package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2017 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import net.welen.jmole.collector.MBeanCollector;
import net.welen.jmole.finder.MBeanFinder;
import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.threshold.Threshold;

public class ConfigurationTest {
	
	@Test
	public void testSetLevel() {
		Configuration configuration = new Configuration();
		configuration.setLevel(3);
		assertEquals(3, configuration.getLevel());
	}

	@Test(expected=RuntimeException.class)
	public void testSetToLowLevel() {
		new Configuration().setLevel(0);
	}

	@Test(expected=RuntimeException.class)
	public void testSetToHighLevel() {
		new Configuration().setLevel(6);
	}

	@Test
	public void testSetMBeanFinder() {
		Configuration config = new Configuration();
		MBeanFinder finder = Mockito.mock(MBeanFinder.class);		
		config.setMBeanFinder(finder);
		assertSame(finder, config.getMBeanFinder());
	}

	@Test
	public void testSetMBeanCollector() {
		Configuration config = new Configuration();
		MBeanCollector collector = Mockito.mock(MBeanCollector.class);		
		config.setMBeanCollector(collector);
		assertSame(collector, config.getMBeanCollector());
	}

	@Test
	public void testSetPresentationInformation() {
		Configuration config = new Configuration();
		PresentationInformation presentationInformation = Mockito.mock(PresentationInformation.class);		
		config.setPresentationInformation(presentationInformation);
		assertSame(presentationInformation, config.getPresentationInformation());
	}

	@Test
	public void testSetThresholds() {
		Configuration config = new Configuration();
		Map<String, Threshold> thresholds = new HashMap<String, Threshold>();			
		config.setThresholds(thresholds);
		assertSame(thresholds, config.getThresholds());
	}

}
