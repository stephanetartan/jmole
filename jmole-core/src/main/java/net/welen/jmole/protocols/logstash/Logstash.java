package net.welen.jmole.protocols.logstash;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2017 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;
import javax.net.ssl.SSLSocketFactory;

import com.google.gson.Gson;

import net.welen.jmole.JMole;
import net.welen.jmole.presentation.PresentationInformation;

public class Logstash implements LogstashMBean, Runnable {

	private final static Logger LOG = Logger.getLogger(Logstash.class.getName());

	private static final String SEP = "/";

	private static String PROPERTY_LOGSTASH_ENABLED = "jmole.protocol.logstash.enabled";
	private static String PROPERTY_LOGSTASH_USESSL = "jmole.protocol.logstash.useSSL";
	private static String PROPERTY_LOGSTASH_HOST = "jmole.protocol.logstash.host";
	private static String PROPERTY_LOGSTASH_PORT = "jmole.protocol.logstash.port";
	private static String PROPERTY_LOGSTASH_KEEPALIVE = "jmole.protocol.logstash.keepAlive";
	private static String PROPERTY_LOGSTASH_INTERVAL = "jmole.protocol.logstash.interval";
	
	private Boolean useSSL;
	private String host;
	private Integer port;
	private Long interval;
	private Boolean keepAlive;

	private boolean stopped = false;
	private boolean logstashStopped = false;
	private Thread collector;
	private JMole jmole;

	private Socket socket;
	
	@Override
	public boolean isEnabled() {
		return Boolean.getBoolean(PROPERTY_LOGSTASH_ENABLED);
	}
	
	@Override
	public void startProtocol(JMole jmole) throws Exception {
		this.jmole = jmole;
	
		useSSL = Boolean.getBoolean(PROPERTY_LOGSTASH_USESSL);
		if (useSSL == null) {
			useSSL = false;
		}

		host = System.getProperty(PROPERTY_LOGSTASH_HOST);
		if (host == null) {
			host = "localhost";
		}
		
		port = Integer.getInteger(PROPERTY_LOGSTASH_PORT);
		if (port == null) {
			port = 5000;
		}
				
		interval = Long.getLong(PROPERTY_LOGSTASH_INTERVAL);
		if (interval == null) {
			interval = 60000L;
		}

		keepAlive = Boolean.getBoolean(PROPERTY_LOGSTASH_KEEPALIVE);
		if (keepAlive == null) {
			keepAlive = false;
		}

		collector = new Thread(this);
		collector.setName("JMole Logstash collector thread");
		collector.start();
		LOG.log(Level.INFO, "JMole Logstash protocol started: " + host + ":" + port + " interval=" + interval);
	}
	

	@Override
	public void stopProtocol() throws Exception {
		LOG.log(Level.INFO, "Stopping JMole Logstash protocol");
		stopped = true;
		collector.interrupt();
		while (!logstashStopped) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				LOG.log(Level.FINE, e.getMessage(), e);
			}
		}
		LOG.log(Level.INFO, "JMole Logstash protocol stopped");
	}

	@Override
	public boolean useSSL() {
		return useSSL;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public boolean getKeepAlive() {	
		return keepAlive;
	}

	@Override
	public long getInterval() {
		return interval;
	}

	@Override
	public void setInterval(long interval) {
		this.interval = interval;
	}

	@Override
	public void run() {
		stopped  = false;
		logstashStopped = false;
		try {
			while (!stopped) {				
				try {
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					LOG.log(Level.FINE, e.getMessage(), e);
				}

				if (stopped) {
					return;
				}
				
				try {
					collectMeasurements();
					sendWarnings();
					sendCriticals();					
				} catch (Exception e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				} finally {
					if (!keepAlive) {
						try {
							if (socket != null) {
								socket.close();
							}
						} catch (IOException e) {
							LOG.log(Level.WARNING, "Couldn't close socket.", e);
						}
						socket = null;
					}					
				}				
			}
		} finally {		
			logstashStopped = true;
		}
	}

	private void collectMeasurements() throws InstanceNotFoundException, AttributeNotFoundException, ReflectionException, MBeanException, IOException {
		Map<Object, PresentationInformation> presentationInformationMap = new HashMap<Object, PresentationInformation>();
		Gson gson = new Gson();
		for (Entry<String, List<Map<String, Map<String, Object>>>> categoryEntry : jmole.collectMeasurements(presentationInformationMap).entrySet()) {
			Iterator<Map<String, Map<String, Object>>> iter = categoryEntry.getValue().iterator();
			while (iter.hasNext()) {
				for (Entry<String, Map<String, Object>> nameEntry : iter.next().entrySet()) {
					for (Entry<String, Object> attributeEntry : nameEntry.getValue().entrySet()) {

						String piKey = categoryEntry.getKey() + nameEntry.getKey();
						PresentationInformation presentationInformation = presentationInformationMap.get(piKey);
						if (presentationInformation == null) {
							LOG.severe("No presentation information found for: " + piKey + ", Skipping it");
							continue;
						}
						
						try {
							if (attributeEntry.getValue() != null) {
								sendToLogstash(gson.toJson(new LogstashMessage("measurement", null, categoryEntry.getKey(), nameEntry.getKey(), attributeEntry.getKey(), attributeEntry.getValue(), presentationInformation)));																
							}
						} catch (NumberFormatException e) {
							LOG.log(Level.SEVERE, e.getMessage() + ": " + categoryEntry.getKey() + SEP
									+ nameEntry.getKey() + SEP + attributeEntry.getKey(), e);
						}						
					}
				}
			}
		}
	}
	
	private void sendWarnings() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		Gson gson = new Gson();
		for ( Entry<String, Map<String, String>> categoryEntry : jmole.warningMessages().entrySet()) {
			for (Entry<String, String> entry : categoryEntry.getValue().entrySet()) {
				sendToLogstash(gson.toJson(new LogstashMessage("warning", entry.getValue(), categoryEntry.getKey(), entry.getKey(), null, null, null)));
			}
		}
	}

	private void sendCriticals() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		Gson gson = new Gson();
		for ( Entry<String, Map<String, String>> categoryEntry : jmole.criticalMessages().entrySet()) {
			for (Entry<String, String> entry : categoryEntry.getValue().entrySet()) {				
				sendToLogstash(gson.toJson(new LogstashMessage("critical", entry.getValue(), categoryEntry.getKey(), entry.getKey(), null, null, null)));
			}
		}
	}

	private void sendToLogstash(String data) {					
		LOG.log(Level.FINE, "Sending data to logstash: " + data);
		PrintWriter out = null;
		try {
			out = new PrintWriter(getSocketOutputStream());
			out.write(data + "\n");
			out.flush();
		} catch (IOException e) {			
			socket = null;		
			LOG.log(Level.SEVERE, "Couldn't send data to logstash: " + data, e);
		}	
	}
	
	private OutputStream getSocketOutputStream() throws UnknownHostException, IOException {
		if (socket == null) {
			if (useSSL) {				
				socket = SSLSocketFactory.getDefault().createSocket(host, port);
			} else {
				socket = new Socket(host, port);
			}
		}
		return socket.getOutputStream();
	}

}
