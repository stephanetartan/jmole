package net.welen.jmole.protocols;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import net.welen.jmole.presentation.PresentationInformation;
import net.welen.jmole.protocols.logger.Logger;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2017 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

public class Utils {
	
	private final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(Utils.class.getName());
	
	/**
	 * 
	 * @param format
	 * @param sep
	 * @param categoryEntry
	 * @param nameEntry
	 * @param attributeEntry
	 * @param presentationInformation
	 *
	 * @throws NumberFormatException
	 * 
	 * @return
	 */
	public static String formatLogString(String format, char sep,
			Entry<String, List<Map<String, Map<String, Object>>>> categoryEntry,
			Entry<String, Map<String, Object>> nameEntry, Entry<String, Object> attributeEntry,
			PresentationInformation presentationInformation) {

		String answer = format;

		// %k
		String key = String.format("%s%c%s%c%s", categoryEntry.getKey(), sep, nameEntry.getKey(),
				sep, attributeEntry.getKey());
		answer = answer.replaceAll("%k", key);
		// %a
		answer = answer.replaceAll("%a", presentationInformation.translateAttributeLabel(attributeEntry.getKey()));
		// %A
		String attributeDescription = presentationInformation.getAttributeDescription(attributeEntry.getKey());
		answer = answer.replaceAll("%A", attributeDescription != null ? attributeDescription : "");
		// %v
		try {
			answer = answer.replaceAll("%v",
					new BigDecimal(attributeEntry.getValue().toString()).toPlainString());
		} catch (NumberFormatException e) {
			LOG.log(Level.FINE, e.getMessage() + ": " + key, e);
			answer = answer.replaceAll("%v", attributeEntry.getValue().toString());			
		}						
		
		// %U
		answer = answer.replaceAll("%U", presentationInformation.getUnit());

		return answer.toString();
	}
	
}
