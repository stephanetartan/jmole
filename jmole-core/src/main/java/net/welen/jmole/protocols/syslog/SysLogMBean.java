package net.welen.jmole.protocols.syslog;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2017 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import net.welen.jmole.protocols.Protocol;

public interface SysLogMBean extends Protocol {
	
	public boolean getUseTCP();
	public String getMessageHostName();	
	public String getApplicationName();	
	public String getHost();
	public int getPort();
	public String getFormat();
	public String getLogFormat();
	public void setLogFormat(String format);
	public boolean getUseSSL();	
	public long getInterval();
	public void setInterval(long interval);
	
}
