package net.welen.jmole.protocols.logstash;

import net.welen.jmole.presentation.PresentationInformation;

public class LogstashMessage {
	private String type;
	private String message;
	private String category;
	private String name;
	private String attribute;
	private Object value;
	private String unit;
	private String label;
	private String description;

	public LogstashMessage(String type, String message, String category, String name, String attribute, Object value, PresentationInformation presentationInformation) {
		this.type = type;
		this.message = message;
		this.category = category;
		this.name = name;
		this.attribute = attribute;
		this.value = value;
		
		this.unit = presentationInformation.getUnit();
		this.label = presentationInformation.getAttributeLabel(attribute);
		this.description = presentationInformation.getAttributeDescription(attribute);		
	}

}
