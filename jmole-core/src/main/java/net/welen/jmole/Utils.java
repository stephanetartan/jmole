package net.welen.jmole;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2017 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;

/**
 * Collection of util methods that are used through JMole
 */
public class Utils {
	
    private final static Logger LOG = Logger.getLogger(Utils.class.getName());

	protected final static String PROPERTY_MBEAN_SERVER = "jmole.mbeanserver.agentid";

	/**
	 * Get the MBean server to use. Uses a system property to choose which
	 * MBeanServer to use if the default platform one shouldn't be used.
	 * 
	 * @return The MBeanServer to use
	 */
	public static MBeanServer getMBeanServer() {
		String agentId = System.getProperty(PROPERTY_MBEAN_SERVER);
		 
		if (agentId == null) {
			return ManagementFactory.getPlatformMBeanServer();
		}
		
		ArrayList<MBeanServer> servers = MBeanServerFactory.findMBeanServer(null);
		for (MBeanServer server : servers) {
			try {
				if (server.getAttribute(new ObjectName("JMImplementation:type=MBeanServerDelegate"), "MBeanServerId").toString().matches(agentId)) {
					return server;
				}
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Can't get the AgentId", e);
			}
		}

		List<String> agentIds = new ArrayList<String>();			
		for (MBeanServer server : servers) {
			try {
				agentIds.add(server.getAttribute(new ObjectName("JMImplementation:type=MBeanServerDelegate"), "MBeanServerId").toString());
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Can't get the AgentId", e);
			}
		}			
		throw new IllegalArgumentException("MBean server with id: " + agentId + " not found. Available id: " + agentIds);
	}
	
	static public String rpnCalculate(String expr) {		
		if (expr == null || expr.isEmpty()) {
			return "";
		}
		Stack<Double> stack = new Stack<Double>();
		String pieces[] = expr.split(",");
		for (int i = 0; i < pieces.length; i++) {
			String value = pieces[i].trim();
			if (value.equals("+")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue + lastValue);
			} else if (value.equals("-")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue - lastValue);
			} else if (value.equals("*")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue * lastValue);
			} else if (value.equals("/")) {
				Double lastValue = stack.pop();
				Double lastLastValue = stack.pop();
				stack.push(lastLastValue / lastValue);
			} else {
				stack.push(Double.valueOf(pieces[i]));
			}
		}
		double answer = stack.pop();
					
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		DecimalFormat formatter = new DecimalFormat("#.#", symbols); 
											
		return formatter.format(answer);
	}

}
