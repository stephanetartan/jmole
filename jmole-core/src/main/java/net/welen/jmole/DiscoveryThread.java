package net.welen.jmole;

import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2017 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

public class DiscoveryThread implements Runnable {

	private final static Logger LOG = Logger.getLogger(DiscoveryThread.class.getName());
	
	private JMole jmole;

	private boolean performDiscovery;

	private boolean stopped = false;
	
	public DiscoveryThread(JMole jmole) {
		this.jmole = jmole;
	}
	
	@Override
	public void run() {
		while (!stopped) {
			while (performDiscovery) {
				try {
					performDiscovery = false;					
					jmole.discover();					
				} catch (Throwable t) {
					LOG.log(Level.SEVERE, "Error during discovery", t);
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// Ignore
			}
		}
	}

	public void performDiscovery() {
		performDiscovery = true;
	}
	
	public void stop() {
		stopped = true;
	}
}
