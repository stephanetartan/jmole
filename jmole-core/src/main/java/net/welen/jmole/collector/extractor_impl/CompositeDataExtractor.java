package net.welen.jmole.collector.extractor_impl;

/*
 * #%L
 * JMole, https://bitbucket.org/awelen/jmole
 * %%
 * Copyright (C) 2015 - 2017 Anders Welén, anders@welen.net
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.management.openmbean.CompositeData;

import net.welen.jmole.collector.DataCollectorExtractor;


public class CompositeDataExtractor implements DataCollectorExtractor {

	private String fakeAttribute = null;
	private String realAttribute = null;
	private String attribute = null;
	
	@Override
	public void setFakeAttribute(String attribute) {
		this.fakeAttribute = attribute;
	}
	
	@Override
	public String getFakeAttribute() {
		return fakeAttribute;
	}
	
	@Override
	public void setRealAttribute(String attribute) {
		this.realAttribute = attribute;
	}
	
	@Override
	public String getRealAttribute() {
		return realAttribute;
	}
	
	@Override
	public Object extractData(Object dataSource) {
		if (dataSource instanceof CompositeData) {
			CompositeData compositeData = (CompositeData) dataSource;
			return compositeData.get(fakeAttribute);
		} else {
			throw new IllegalArgumentException("Illegal class");
		}
	}

	@Override
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	@Override
	public String getAttribute() {
		return attribute;
	}
}
